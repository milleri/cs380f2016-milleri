drop table Voters;
create table Voters (
      Identification varchar(5) primary key,
      Voted    varchar(30) not null,
      Gender    varchar(20),
      Age       varchar(20),
      Ethnicity varchar(20),
      Religion varchar(20),
      Income varchar(25));
