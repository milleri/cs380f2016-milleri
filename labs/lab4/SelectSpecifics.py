#!/usr/bin/env python
# 13 Oct 2016
# Written by Oliver Bonham-Carter following the tutorial at
# http://sebastianraschka.com/Articles/2014_sqlite_in_python_tutorial.html
# email: obonhamcarter@allegheny.edu

# runs, "select * from instructor;" using python

import sqlite3

sqlite_file = "campus_ii.sqlite3" # the database file.

#table_name = 'instructor'   # name of the table to be queried
#id_column = 'ID'
#some_id = 10110
#column_2 = 'name'
#column_3 = 'student'

# Connecting to the database file
conn = sqlite3.connect(sqlite_file)
c = conn.cursor()


########################################################################################
# 1) Contents of all columns for row that match a certain value in 1 column
# select * from instructor where name = "Nelson"; 
# Output:
# 10111|Nelson|S5|CompBio|103000
table_name = 'course'
table_name1 = 'teaches'
table_name2 = 'department'
table_name3 = 'instructor'
table_name4 = 'student'
table_name5 = 'takes'
column_1 = 'title'
column_2 = 'name'
column_3 = 'deptName'
column_4 = 'courseId'
column_5 = 'grade'
column_6 = 'ID'#c.execute('SELECT * FROM {tn} WHERE {cn}="Nelson" '.format(tn=table_name, cn=column_2))
#all_rows = c.fetchall()
#print('1):', all_rows)
c.execute('Select * from {tn} where {cn} = "Databases"'. format(tn = table_name, cn = column_1))
all_rows = c.fetchall()
print('1a):  ', all_rows)
c.execute('Select * from department where {cn} = "Math"'.format(cn = column_3))
all_rows = c.fetchall()
print('1b): ', all_rows)
c.execute('Select * from instructor where {cn} = "Nelson"'. format(cn = column_2))
all_rows = c.fetchall()
print('1c) :' , all_rows)
c.execute('Select * from student where {cn} = "Beuller"'. format(cn = column_2))
all_rows = c.fetchall()
print('1d):' , all_rows)
c.execute('Select * from takes where {cn4} = "MTH202"'. format(cn4 = column_4))
allrows = c.fetchall()
print('1e):' , allrows)
c.execute('Select * from {tb1} where semester = "Fall"'.format(tb1 = table_name1))
allrows = c.fetchall()
print('1f):', allrows)



####################table_name = 'course'
####################################################################
# 2) Value of a particular column for rows that match a certain value in column_1
# select name from instructor where student ="S1";
# Output:
# Miller
# Johnson
# Wu


#c.execute('SELECT ({attrib}) FROM {tn} WHERE {c}="S1"' .format(attrib=column_2, tn=table_name, c=column_3))
#all_rows = c.fetchall()
#print('2):', all_rows)
c.execute('Select title from {tb} where {cn4} = "CS100"' .format(tb = table_name, cn4 = column_4))
allrows = c.fetchall()
print('2a):', allrows)
c.execute('Select {cn3} from department where courseType = "Mathematics"' . format(cn3 = column_3))
allrows = c.fetchall()
print('2b):', allrows)
c.execute('Select {cn2} from instructor where salary = 87000'.format(cn2 = column_2))
allrows = c.fetchall()
print('2c):', allrows)
c.execute('Select {cn2} from student where ID = "xS12"' .format(cn2 = column_2))
allrows = c.fetchall()
print('2d):', allrows)
c.execute('Select secId from takes where year = 2010')
allrows = c.fetchall()
print('2e):', allrows)
c.execute('Select ID from {tb1} where semester ="Spring"'. format(tb1 = table_name1))
allrows = c.fetchall()
print('2f):', allrows)






########################################################################################

# 3) Value of 2 particular columns for rows that match a certain value in 1 column
# select name, student from instructor where name == "Watson";
# Output:
# Watson|S4

#c.execute('SELECT {attrib1},{attrib2} FROM {tn} WHERE {attrib1}="Watson"'.format(attrib1=column_2, attrib2=column_3, tn=table_name, cn=column_2))
#all_rows = c.fetchall()
#print('3):', all_rows)
c.execute('Select {cn1}, {cn3} from {tb} where {cn1} = "Graphics"'.format(cn1 = 'title', cn3 = column_3, tb = table_name))
allrows = c.fetchall()
print('3a):', allrows)
c.execute('Select {cn3}, courseType from department where {cn3} = "Math"'.format(cn3 = column_3))
allrows = c.fetchall()
print('3b):', allrows)
c.execute('Select {cn2}, salary from instructor where salary = 86000'.format(cn2 = column_2))
allrows = c.fetchall()
print('3c):', allrows)
c.execute('Select {cn2}, {cn3} from student where name = "Beuller"'.format(cn2 = column_2, cn3= column_3))
allrows = c.fetchall()
print('3d):', allrows)
c.execute('Select {cn}, semester from takes where {cn} = "MTH202"' .format(cn=column_4))
allrows = c.fetchall()
print('3e):', allrows)
c.execute('Select ID, {cn} from {tb} where ID = "12362"'.format(cn = column_4,tb = table_name1))
allrows = c.fetchall()
print('3f):', allrows)



########################################################################################

# 4) Selecting only up to three rows that match a certain value in 1 column
# SELECT * FROM instructor where ID like "101%" limit 3;
# Output:
# 10101|Miller|S1|CompSci|95000
# 10102|Johnson|S1|CompSci|95400
# 10103|Charleson|S2|CompSci|96000

c.execute('SELECT * FROM {tn} WHERE {cn4} like "CS%" limit 3'.format(tn=table_name, cn4=column_4))
allrows = c.fetchall()
print('4a):', allrows)
c.execute('Select * from {tn} where {cn3} like "comp%" limit 3'.format(tn = 'department', cn3 = column_3))
allrows = c.fetchall()
print('4b):', allrows)
c.execute('Select * from {tn} WHERE courseId like "101%" limit 3' .format(tn = table_name2))
three_rows = c.fetchall()
print('4c):', allrows)
c.execute('Select * from {tn} where {cn4} like "Comp%" limit 3'.format(tn = 'student', cn4 = column_3))
allrows = c.fetchall()
print('4d):', allrows)
c.execute('Select * from {tn} where {cn4} like "MTH%" limit 3'.format(tn = 'takes', cn4 = column_4))
allrows = c.fetchall()
print('4e):', allrows)
c.execute('Select * from {tn} where {cn4} like "Eng%" limit 3'.format(tn = 'teaches', cn4 = column_4))
allrows = c.fetchall()
print('4f):', allrows)









########################################################################################

# 5) Selecting names where the double-l is two letters in from both end.
# SELECT * FROM instructor where Name like "__ll__";
# Output:
# 10101|Miller|S1|CompSci|95000

c.execute('Select * from {tn} where {cn4} like "__6__"'.format(tn= table_name, cn4 = column_4))
allrows = c.fetchall()
print('5a):', allrows)
c.execute('Select * from {tn} where {cn4} like "___101"'.format(tn = 'department', cn4 = column_4))
allrows = c.fetchall()
print('5b):', allrows)
c.execute('SELECT * FROM {tn} WHERE {cn2} like "__ll__"'.format(tn='instructor', cn2=column_2))
allrows = c.fetchall()
print('5c):', allrows)
c.execute('Select * from {tn} where {cn2} like "___tt_"'.format(tn = 'student', cn2 = column_2))
allrows = c.fetchall()
print('5d):', allrows)
c.execute('Select * from {tn} where {cn4} like "___2__"'.format(tn = 'takes', cn4 = column_4))
allrows = c.fetchall()
print('5e):', allrows)
c.execute('Select * from {tn1} where {cn4} like "Eng____"'.format(tn1 = table_name1, cn4 = column_4))
allrows = c.fetchall()
print('5f):', allrows)




########################################################################################








# 7) Check if a certain ID exists and print its column content if so, 
#    otherwise, print out that the id does not exist
# select * from instructor where Id ="007";
# Output: 
# - nothing -
c.execute("select * from {} where {} == 'CompSci'".format(table_name, column_3))
all_rows = c.fetchall()
print('7a:', all_rows)

c.execute("select * from {} where {} == 'CS-101'".format(table_name1, column_4))
all_rows = c.fetchall()
print('7b):', all_rows)

c.execute("select * from {} where {} == 'Math'".format(table_name2, column_3))
all_rows = c.fetchall()
print('7c):', all_rows)

c.execute("select * from {} where {} == 'Biology'".format(table_name3, column_3))
all_rows = c.fetchall()
print('7d):', all_rows)

c.execute("select * from {} where {} == 'Ralph'".format(table_name4, column_2))
all_rows = c.fetchall()
print('7e):', all_rows)

c.execute("select * from {} where {} == 'CS101'".format(table_name5, column_4))
all_rows = c.fetchall()
print('7f):', all_rows)



# 8) change an entry using UPDATE.

# 10112|Farber|S5|CompBio|101000
# UPDATE instructor set salary == "101" where name == "Farber";
# 10112|Farber|S5|CompBio|101
c.execute("update {} set {} == 'LIES' where {} == 'CS600'".format(table_name, column_3, column_4))
c.execute('Select {cn3} from {tb} where {cn4} == "CS600"'.format(tb =  table_name, cn3 = column_3, cn4 = column_4)) 
allrows = c.fetchall()
print('8a(before)):', allrows)
c.execute("update {} set {} == 'SNEEZES' where {} == 'CS600'".format(table_name, column_3, column_4))
c.execute('Select {cn3} from {tb} where {cn4} == "CS600"'.format(tb =  table_name, cn3 = column_3, cn4 = column_4)) 
allrows = c.fetchall()
print('8a(after)', allrows)
c.execute('Select {cn3} from {tb} where {cn3} == "Bio" and {cn4} == "CS600"'.format(tb =  table_name, cn3 = column_3, cn4 = column_4)) 
allrows = c.fetchall()
#print('First', allrows)


c.execute("update {} set {} == 'CARRIED' where {} == '12365'".format(table_name1, column_4, column_6))
c.execute("select * from {} where {} == '12365'".format(table_name1, column_6))
all_rows = c.fetchall()
print('8b(before):', all_rows)
c.execute("update {} set {} == 'Eng-210' where {} == '12365'".format(table_name1, column_4, column_6))
c.execute("select * from {} where {} == '12365'".format(table_name1, column_6))
all_rows = c.fetchall()
print('8b(after)):', all_rows)



c.execute("update {} set {} == 'BLOP' where {} == 'BIO101'".format(table_name2, column_3, column_4))
c.execute("select * from {} where {} == 'BIO101'".format(table_name2, column_4))
all_rows = c.fetchall()
print('8c(before):', all_rows)
c.execute("update {} set {} == 'Bio' where {} == 'BIO101'".format(table_name2, column_3, column_4))
c.execute("select * from {} where {} == 'BIO101'".format(table_name2, column_4))
all_rows = c.fetchall()
print('8c(after)):', all_rows)


c.execute("update {} set {} == 'SNORES' where {} == 'Miller'".format(table_name3, column_3, column_2))
c.execute("select * from {} where {} == 'Miller'".format(table_name3, column_2))
all_rows = c.fetchall()
print('8d(before):', all_rows)
c.execute("update {} set {} == 'Math' where {} == 'Miller'".format(table_name3, column_3, column_2))
c.execute("select * from {} where {} == 'Miller'".format(table_name3, column_2))
all_rows = c.fetchall()
print('8d(after)):', all_rows)




c.execute("update {} set {} == 'MALORIE' where {} == 'S1'".format(table_name4, column_2, column_6))
c.execute("select * from {} where {} == 'S1'".format(table_name4, column_6))
all_rows = c.fetchall()
print('8e(before):', all_rows)
c.execute("update {} set {} == 'Miller' where {} == 'S1'".format(table_name4, column_2, column_6))
c.execute("select * from {} where {} == 'S1'".format(table_name4, column_6))
all_rows = c.fetchall()
print('8e(after)):', all_rows)



c.execute("update {tn} set {cn} == 'ABBA' where {cn1} == '12348'".format(tn = table_name5, cn = column_5, cn1 = column_6))
c.execute("select * from {} where {} == '12348'".format(table_name5, column_6))
all_rows = c.fetchall()
print('8f(before):', all_rows)
c.execute("update {tn} set {cn} == 'A' where {cn1} == '12348'".format(tn = table_name5, cn = column_5, cn1 = column_6))
c.execute("select * from {} where {} == '12348'".format(table_name5, column_6))
all_rows = c.fetchall()
print('8f(after)):', all_rows)
# Check Farber's salary before...





# Check Farber's salary after...


# Committing changes and closing the connection to the database file
conn.commit()

# set the whole record back to its original values.

# Check Farber's salary after...


# Committing changes and closing the connection to the database file
conn.commit()







# Closing the connection to the database file
conn.close()
 
